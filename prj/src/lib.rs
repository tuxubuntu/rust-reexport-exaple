
#[cfg(test)]
mod tests {
	use re::serde::{Deserialize, Serialize};
	#[derive(Debug, PartialEq, Deserialize, Serialize)]
	pub struct Info {}
	#[test]
	fn it_works() {
		let _ = Info {};
		assert_eq!(2 + 2, 4);
	}
}
